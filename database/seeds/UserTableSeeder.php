<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$row = \DB::Table('users')->first();
    	if(!$row){
        	\DB::Table('users')->insert(
        		[
        			'name'=>"Admin",
        			'email'=>"admin@admin.com",
        			'password'=>bcrypt('111111')
        		]
        	);
    	}
    }
}
