<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{$title or "Dashboard"}}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{url('assets/css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{url('assets/css/light-bootstrap-dashboard.css')}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{url('assets/css/demo.css')}}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{url('assets/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
    <link href="{{url('assets/admin/style.css')}}" rel="stylesheet" />

</head>
<body>

    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="{{url('assets/img/sidebar-5.jpg')}}">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{route('admin.home')}}" class="simple-text">
                Nepal News
            </a>
        </div>

        <ul class="nav">
            <li class="{{isActive('admin.home')}}">
                <a href="{{route('admin.home')}}">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{isActive('admin.category')}}">
                <a href="{{route('admin.category')}}">
                    <i class="pe-7s-albums"></i>
                    <p>Categories</p>
                </a>
            </li>
            <li class="{{isActive('admin.post')}}">
                <a href="{{route('admin.post')}}">
                    <i class="pe-7s-note2"></i>
                    <p>Posts</p>
                </a>
            </li>
            <li class="{{isActive('admin.post.type')}}">
                <a href="{{route('admin.post.type',['type'=>'page'])}}">
                    <i class="pe-7s-browser"></i>
                    <p>Pages</p>
                </a>
            </li>
            <li class="{{isActive('admin.gallery')}}">
                <a href="{{route('admin.gallery')}}">
                    <i class="pe-7s-photo"></i>
                    <p>Gallery</p>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('admin.home')}}">Dashboard</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('home')}}" target="_blank">Visit Site</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">Welcome {{Auth::user()->name}}</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="content">
        @yield('content')
    </div>


    <footer class="footer">
        <div class="container-fluid">
            <p class="copyright pull-right">
                &copy; 2017
            </p>
        </div>
    </footer>

</div>
</div>


<!--   Core JS Files   -->
<script src="{{url('assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{url('assets/js/bootstrap-checkbox-radio-switch.js')}}"></script>

<!--  Charts Plugin -->
<script src="{{url('assets/js/chartist.min.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{url('assets/js/bootstrap-notify.js')}}"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="{{url('assets/js/light-bootstrap-dashboard.js')}}"></script>
<script src="{{url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="{{url('assets/js/demo.js')}}"></script>
@yield('scripts')
</body>
</html>
