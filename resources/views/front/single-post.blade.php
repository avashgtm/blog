@extends('front.layouts.master')
@section('header')
<div class="blog-header">
  <h1 class="blog-title">{{$post->title}}</h1>
</div>
@stop
@section('content')
<div class="blog-post single">
 
  <p class="blog-post-meta">{{$post->created_at}} by <em>Mark</em>
    , Category - <strong>{{$post->category->name}}</strong>
  </p>
  @if($post->featured)
  <p>
   <img src="{{asset($post->featured)}}" class="img-responsive">
 </p>
 @endif
 @if($post->doc)
  <br>
  <a href='{{asset($post->doc)}}'target="_blank">View Doc</a>
@endif
 {!!$post->content!!}
</div><!-- /.blog-post -->
@endsection
