<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Category as Model;

class CategoryController extends Controller
{
    protected $model;
    public function __construct(Model $model)
    {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index()
    {
        $categories = $this->model->orderBy('weight','desc')->orderBy('created_at','desc')->get();
        $title = "Categories List";
        return view('admin.category.index')->with(['categories'=>$categories,'title'=>$title]);
    }
    public function create()
    {
        $title = "Create Category";
        return view('admin.category.create')->with(['title'=>$title]);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required'
            ]);

        $this->model->name = trim($request->name);
        $this->model->front = $request->front?1:0;
        $this->model->save();

        return redirect()->route('admin.category')->with(['success'=>"Category Created"]);
    }
    public function edit($id)
    {
        $title = "Edit Category";
        $category = $this->model->find($id);
        return view('admin.category.edit')->with(compact('title','category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required'
            ]);

        $this->model = $this->model->find($id);
        $this->model->name = trim($request->name);
        $this->model->front = $request->front?1:0;
        $this->model->weight = $request->weight;
        $this->model->save();

        return redirect()->route('admin.category')->with(['success'=>"Category Updated"]);
    }
    public function destroy($id)
    {
        $this->model->find($id)->delete();
        return redirect()->route('admin.category')->with(['success'=>"Category Deleted"]);
    }
}
