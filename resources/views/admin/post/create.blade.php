@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <form method="post" action="{{route('admin.post.store')}}" enctype="multipart/form-data">
        <input type="hidden" name="type" value="{{$type}}">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{$title}}</h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="content">
                        @include('admin.includes.errors')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{old('title')}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Content</label>
                                    <textarea rows="10" class="form-control" placeholder="Type Content" name="content" id="content">{{old('content')}}</textarea>
                                </div>
                            </div>
                            @if($type=="post")
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Excerpt</label>
                                    <textarea rows="3" class="form-control" placeholder="Type Excerpt" name="excerpt">{{old('excerpt')}}</textarea>
                                </div>
                            </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <select class="form-control" name="status">
                                        <option value="publish">Publish</option>
                                        <option value="draft">Draft</option>
                                    </select>
                                </div>
                            </div>
                            @if($type=="post")
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Categories</label>
                                    <select class="form-control" name="category_id">
                                        <option value="">Select</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Featured Image</label>
                                    <input type="file" name="featured" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Document(pdf,doc)</label>
                                    <input type="file" name="doc" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
@stop
@section('scripts')
<script>
  tinymce.init({
    selector: '#content'
});
</script>
@endsection