<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Gallery as Model;

class GalleryController extends Controller
{
    protected $model;
    public function __construct(Model $model)
    {
        $this->middleware('auth');
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $images = $this->model->orderBy('id','desc')->get();
        $title = "Manage Gallery";
        return view('admin.gallery.index')->with(['images'=>$images,'title'=>$title]);
    }

    public function store(Request $request)
    {
        $rules = [
        'caption'=>'required',
        'image'=>'required|image'
        ];
        $this->validate($request,$rules);

        $this->model->caption = trim($request->caption);

        if(isset($rules['image'])){
            $path = $request->image->store('images/gallery');
            $this->model->image = $path;
        }
        $this->model->save();

        return redirect()->route('admin.gallery')->with(['success'=>"Image Created"]);
    }
    public function edit($id)
    {
        $image = $this->model->find($id);
        $title = "Edit Image";
        return view('admin.gallery.edit')->with(compact('title','image'));
    }
    public function update(Request $request, $id)
    {
        $rules = [
        'caption'=>'required',
        'weight'=>'required',
        ];
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $rules['image'] = "image";
        }
        $this->validate($request,$rules);

        $this->model = $this->model->find($id);

        $this->model->caption = trim($request->caption);
        $this->model->weight = trim($request->weight);
        if(isset($rules['image'])){
            $path = $request->image->store('images/gallery');
            if(file_exists(public_path($this->model->image))){
                unlink(public_path($this->model->image));
            }
            $this->model->featured = $path;
        }
        $this->model->save();



        return redirect()->route('admin.gallery')->with(['success'=>"Image Updated"]);
    }
    public function destroy($id)
    {
        $image = $this->model->find($id);
        if($image->image){
          if(file_exists(public_path($image->image))){
            unlink(public_path($image->image));
        }           
    }
    $image->delete();
    return redirect()->route('admin.gallery')->with(['success'=>"Image Deleted"]);
}
}
