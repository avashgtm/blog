<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nepal News - {{$title or ''}}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('assets/front.css')}}" rel="stylesheet">
  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item {{isActive('home')}}" href="{{route('home')}}">Home</a>
          @foreach($catmenus as $menu)
            @if($menu->front)
              <a href="{{route('category.news',['id'=>$menu->id])}}" class="blog-nav-item {{isActiveCat($menu->id)}}">{{$menu->name}}</a>
            @endif
          @endforeach
          <a href="{{url('login')}}" class="blog-nav-item pull-right {{isActive('login')}}">login</a>
          <a href="{{route('gallery')}}" class="blog-nav-item pull-right {{isActive('gallery')}}">gallery</a>

        </nav>
      </div>
    </div>

    <div class="container">

    @yield('header')

      <div class="row">

        <div class="col-sm-8 blog-main">
          @yield('content')
        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>Pages</h4>
            <ul>
              @foreach($sidepages as $page)
                  <li>
                    <a href="{{route('single.page',['id'=>$page->id])}}">{{$page->title}}
                    </a>
                  </li>
              @endforeach
            </ul>
          </div>
          <div class="sidebar-module">
            <h4>Others Categories</h4>
            <ol class="list-unstyled">
              @foreach($catmenus as $menu)
                @if(!$menu->front)
                  <li>
                    <a href="{{route('category.news',['id'=>$menu->id])}}" class="{{isActiveCat($menu->id)}}">{{$menu->name}}</a>
                  </li>
                @endif
              @endforeach
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Social</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <footer class="blog-footer">
      <p>Nepal News @copyright All Rights Reserved
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('assets/js/jquery-1.10.2.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    @yield('scripts')
  </body>
</html>
