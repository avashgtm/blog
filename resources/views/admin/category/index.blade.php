@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @include('admin.includes.success')
            <div class="card">
                <div class="header">
                    <h4 class="title">Categories List</h4>
                    <a href="{{route('admin.category.create')}}" class="btn btn-primary">new</a>
                </div>
                <div class="content table-responsive table-full-width">
                  @if($categories->count())
                  <table class="table table-hover table-striped">
                    <thead>
                       <th>Name</th>
                       <th>Front</th>
                       <th>Order</th>
                       <th></th>
                   </thead>
                   <tbody>
                       @foreach($categories as $category)
                       <tr>
                           <td>{{$category->name}}</td>
                           <td>{{$category->front?"show":"hide"}}</td>
                           <td>{{$category->weight}}</td>
                           <td>
                            <a href="{{route('admin.category.edit',['id'=>$category->id])}}"><i class="fa fa-pencil"></i></a> 
                            <a onClick="return confirm('Are you sure')" href="{{route('admin.category.delete',['id'=>$category->id])}}"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            @else
            <div class="typo-line">
              <h4>No Categories - Please add Some categories</h4>
          </div>
          @endif
      </div>
  </div>
</div>
</div>
</div>
@stop