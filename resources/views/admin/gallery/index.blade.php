@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="header">
                    <h4 class="title">Images List</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="content">
                    @include('admin.includes.errors')
                    <div class="content table-responsive table-full-width">
                      @if($images->count())
                      <table class="table table-hover table-striped">
                        <thead>
                            <th></th>
                            <th>Caption</th>
                            <th>Order</th>
                            <th>Created</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($images as $img)
                            <tr>
                                <td><img src="{{asset($img->image)}}" alt="" class="img-circle" width="100px" height="auto"></td>
                                <td>{{$img->caption}}</td>
                                <td>{{$img->weight}}</td>
                                <td>{{$img->created_at}}</td>
                                <td>
                                    <a href="{{route('admin.gallery.edit',['id'=>$img->id])}}"><i class="fa fa-pencil"></i></a> 
                                    <a onClick="return confirm('Are you sure')" href="{{route('admin.gallery.delete',['id'=>$img->id])}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else
                    <div class="typo-line">
                        <h4>No images - Please add images</h4>
                    </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="header">
                <h4 class="title">Add Image</h4>
                <div class="clearfix"></div>
            </div>
            <form method="post" action="{{route('admin.gallery.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Caption</label>
                                <input type="text" class="form-control" placeholder="Caption" name="caption">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="image" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                        </div>
                        

                    </div>

                </div>
            </div>
        </form>
    </div>
</div>


</div>
@stop