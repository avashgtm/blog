@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="header">
                    <h4 class="title">Create Category</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="content">
                    @include('admin.includes.errors')
                    <form method="post" action="{{route('admin.category.store')}}">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{old('name')}}">
                                </div>
                                <div class="form-group">
                                    <label>Show on menu</label><br>
                                    <input type="checkbox" class="" name="front" checked>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>

</div>
</div>
@stop