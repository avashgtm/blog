<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer('front.layouts.master', function ($view) {
            $catmenus = \App\Models\Category::orderBy('weight','desc')->orderBy('created_at','desc')->get();
            $sidepages = \App\Models\Post::orderBy('id','desc')->where('type','page')->get();
            $view->with(compact('catmenus','sidepages'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
