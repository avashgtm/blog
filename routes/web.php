<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',"FrontController@index")->name('home');
Route::get('category/{id}',"FrontController@categorynews")->name('category.news');
Route::get('post/{id}',"FrontController@single")->name('single.post');
Route::get('page/{id}',"FrontController@single")->name('single.page');
Route::get('gallery',"FrontController@gallery")->name('gallery');
Auth::routes();


Route::group(['prefix'=>'admin'],function(){
	Route::get('/', 'Admin\HomeController@index')->name('admin.home');

	//category
	Route::get('category', 'Admin\CategoryController@index')->name('admin.category');
	Route::get('category/create', 'Admin\CategoryController@create')->name('admin.category.create');
	Route::post('category', 'Admin\CategoryController@store')->name('admin.category.store');
	Route::get('category/{id}/edit', 'Admin\CategoryController@edit')->name('admin.category.edit');
	Route::post('category/{id}', 'Admin\CategoryController@update')->name('admin.category.update');
	Route::get('category/{id}/delete','Admin\CategoryController@destroy')->name('admin.category.delete');

	Route::get('post', 'Admin\PostController@index')->name('admin.post');
	Route::get('post/type/{type}', 'Admin\PostController@index')->name('admin.post.type');
	Route::get('post/create', 'Admin\PostController@create')->name('admin.post.create');
	Route::post('post', 'Admin\PostController@store')->name('admin.post.store');
	Route::get('post/{id}/edit', 'Admin\PostController@edit')->name('admin.post.edit');
	Route::post('post/{id}', 'Admin\PostController@update')->name('admin.post.update');
	Route::get('post/{id}/delete','Admin\PostController@destroy')->name('admin.post.delete');

	Route::get('gallery', 'Admin\GalleryController@index')->name('admin.gallery');
	Route::post('gallery', 'Admin\GalleryController@store')->name('admin.gallery.store');
	Route::get('gallery/{id}/edit', 'Admin\GalleryController@edit')->name('admin.gallery.edit');
	Route::post('gallery/{id}', 'Admin\GalleryController@update')->name('admin.gallery.update');
	Route::get('gallery/{id}/delete','Admin\GalleryController@destroy')->name('admin.gallery.delete');

});
