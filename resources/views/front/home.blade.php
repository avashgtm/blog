@extends('front.layouts.master')
@section('header')
<div class="blog-header">
  <h1 class="blog-title">Home Page</h1>
  <p class="lead blog-description">Recent News</p>
</div>
@stop
@section('content')
@foreach($posts as $post)
<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="{{route('single.post',['id'=>$post->id])}}">
      {{$post->title}}
    </a>
  </h2>
  <p class="blog-post-meta">{{$post->created_at}} by <em>Mark</em>
    , Category - <strong>{{$post->category->name}}</strong>
  </p>
  @if($post->featured)
  <p>
   <img src="{{asset($post->featured)}}" class="img-responsive">
 </p>
 @endif
@if($post->doc)
  <br>
  <a href='{{asset($post->doc)}}'target="_blank">View Doc</a>
@endif
 @if($post->excerpt)
 {{$post->excerpt}}
 @else
 {{makeExcerpt($post->content)}}
 @endif
</div><!-- /.blog-post -->
@endforeach
{{ $posts->links() }}
@endsection
