<?php

function isActive($route){
    if (\Route::currentRouteName() == $route)
    	return 'active';

}
function isActiveCat($id){
	$params = \Route::current()->parameters();
	$iscurrent = isActive('category.news');
	if($iscurrent && $params['id']==$id){
		return 'active';
	}

}
function isActivePostType($type)
{
	$gettype = request()->get('type');
	$iscurrent = isActive('admin.post');
	if($gettype==null)
		return $iscurrent;
	if($iscurrent && $gettype==$type){
		return 'active';
	}
}
function makeExcerpt($content){
	$text = strip_tags($content);
	return implode(' ', array_slice(explode(' ', $text), 0, 20));
}