<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Post as pmodel;
use App\Models\Category as cmodel;
use App\Models\Gallery as gmodel;
class FrontController extends Controller
{
    protected $pmodel;
    protected $cmodel;
    function __construct(pmodel $pmodel, cmodel $cmodel)
    {
        $this->pmodel = $pmodel;
        $this->cmodel = $cmodel;
    }
    public function index()
    {
        $posts = $this->pmodel->with('category')->where('type','post')->where('status','publish')->orderBy('id','desc')->paginate(10);
        $title = "All Nepal News";
        return view('front.home')->with(compact('posts','title'));
    }
    public function Categorynews($id)
    {
        $category = $this->cmodel->find($id);
        $posts = $this->pmodel->where('category_id',$id)->where('status','publish')->orderBy('id','desc')->paginate(10);
        $title = $category->name;
        return view('front.categorynews')->with(compact('posts','title','category'));
    }
    public function single($id)
    {
        $post = $this->pmodel->with('category')->where(['id'=>$id,'status'=>'publish'])->first();
        return view("front.single-".$post->type)->with(compact('post'));
    }
    public function gallery(gmodel $gmodel)
    {
        $images = $gmodel->orderBy('id','desc')->get();
        $title = "gallery";
        return view('front.gallery')->with(compact('images','title'));
    }
}
