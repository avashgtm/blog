@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @include('admin.includes.success')
            <div class="card">
                <div class="header">
                    <h4 class="title">{{$title}}</h4>
                    <a href="{{route('admin.post.create')}}?type={{$type}}" class="btn btn-primary">new</a>
                </div>
                <div class="content table-responsive table-full-width">
                  @if($posts->count())
                  <table class="table table-hover table-striped">
                    <thead>
                       <th>Title</th>
                       <th>Status</th>
                       <th>Created</th>
                       <th></th>
                   </thead>
                   <tbody>
                       @foreach($posts as $post)
                       <tr>
                           <td>{{$post->title}}</td>
                           <td>{{$post->status}}</td>
                           <td>{{$post->created_at}}</td>
                           <td>
                            <a href="{{route('admin.post.edit',['id'=>$post->id])}}"><i class="fa fa-pencil"></i></a> 
                            <a onClick="return confirm('Are you sure')" href="{{route('admin.post.delete',['id'=>$post->id])}}"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
            @else
            <div class="typo-line">
              <h4>No posts/pages - Please add Some posts/pages</h4>
          </div>
          @endif
      </div>
  </div>
</div>
</div>
</div>
@stop