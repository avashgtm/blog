@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-md-8">
            <form method="post" action="{{route('admin.gallery.update',['id'=>$image->id])}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Image</h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="content">
                        @include('admin.includes.errors')

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Caption</label>
                                    <input type="text" class="form-control" placeholder="Caption" name="caption" value="{{$image->caption}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Order</label>
                                    <input type="number" class="form-control" placeholder="Caption" name="weight" value="{{$image->weight}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" name="image" class="form-control"><br>
                                    <img src="{{asset($image->image)}}" alt="" class="img-responsive">
                                </div>
                                <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                            </div>
                            

                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@stop