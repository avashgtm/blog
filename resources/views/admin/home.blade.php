@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <h4 class="title">Categories</h4>
                </div>
                <div class="content">
                    <div>
                        <h2>
                            Count : {{$catscount}}
                        </h2>
                    </div><br><br>

                    <div class="">
                        <a href="{{route('admin.category')}}" class="btn btn-primary">List</a>
                        <a href="{{route('admin.category.create')}}" class="btn btn-success pull-right">New</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <h4 class="title">Posts</h4>
                </div>
                <div class="content">
                    <div>
                        <h2>
                            Count : {{$postscount}}
                        </h2>
                    </div><br><br>

                    <div class="">
                        <a href="{{route('admin.post')}}" class="btn btn-primary">List</a>
                        <a href="{{route('admin.post.create')}}" class="btn btn-success pull-right">New</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <h4 class="title">Pages</h4>
                </div>
                <div class="content">
                    <div>
                        <h2>
                            Count : {{$pagescount}}
                        </h2>
                    </div> <br><br>

                    <div class="">
                        <a href="{{route('admin.post.type',['type'=>'page'])}}" class="btn btn-primary">List</a>
                        <a href="{{route('admin.post.create')}}?type=page" class="btn btn-success pull-right">New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop