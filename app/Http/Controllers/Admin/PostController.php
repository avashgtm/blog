<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Post as Model;

class PostController extends Controller
{
    protected $model;
    public function __construct(Model $model,\App\Models\Category $catmodel)
    {
        $this->middleware('auth');
        $this->model = $model;
        $this->catmodel = $catmodel;
    }

    public function index(Request $request,$type='post')
    {

        $type = $type?$type:$request->get('type','post');
        $posts = $this->model->orderBy('id','desc')->where('type',$type)->get();
        $title = ucwords($type)." List";
        return view('admin.post.index')->with(['posts'=>$posts,'title'=>$title,'type'=>$type]);
    }
    public function create(Request $request)
    {
        $type = $request->get('type','post');
        $title = "Create ".ucwords($type);
        $categories = $this->catmodel->orderBy('id','desc')->get();
        return view('admin.post.create')->with(['title'=>$title,'type'=>$type,'categories'=>$categories]);
    }
    public function store(Request $request)
    {
        $rules = [
        'title'=>'required',
        'status'=>'required'
        ];
        if($request->type=="post"){
            $rules['category_id'] = 'required';
        }
        if($request->hasFile('featured') && $request->file('featured')->isValid()){
            $rules['featured'] = "image";
        }
        if($request->hasFile('doc') && $request->file('doc')->isValid()){
            $rules['doc'] = "mimes:doc,pdf";
        }
        $this->validate($request,$rules);

        $this->model->title = trim($request->title);
        $this->model->content = trim($request->content);
        $this->model->excerpt = trim($request->excerpt);
        $this->model->type = trim($request->type);
        $this->model->status = $request->status;
        $this->model->category_id = $request->category_id;

        $featured = "";
        if(isset($rules['featured'])){
            $path = $request->featured->store('images');
            $featured = $path;
        }

        //upload doc
        $doc = "";
        if(isset($rules['doc'])){
            $path = $request->doc->store('docs');
            $doc = $path;
        }

        $this->model->featured = $featured;
        $this->model->doc = $doc;

        $this->model->user_id = \Auth::user()->id;
        $this->model->save();
        if($request->type=="page"){
            return redirect()->route('admin.post.type',['type'=>'page'])->with(['success'=>"Page Created"]);            
        }
        return redirect()->route('admin.post')->with(['success'=>"Post Created"]);
    }
    public function edit($id)
    {
        $post = $this->model->find($id);
        $title = "Edit ".ucwords($post->type);
        $categories = $this->catmodel->orderBy('id','desc')->get();
        return view('admin.post.edit')->with(compact('title','post','categories'));
    }
    public function update(Request $request, $id)
    {
        $rules = [
        'title'=>'required',
        'status'=>'required'
        ];
        if($request->type=="post"){
            $rules['category_id'] = 'required';
        }
        if($request->hasFile('featured') && $request->file('featured')->isValid()){
            $rules['featured'] = "image";
        }
        if($request->hasFile('doc') && $request->file('doc')->isValid()){
            $rules['doc'] = "mimes:doc,pdf";
        }
        $this->validate($request,$rules);

        $this->model = $this->model->find($id);

        $this->model->title = trim($request->title);
        $this->model->content = trim($request->content);
        $this->model->excerpt = trim($request->excerpt);
        $this->model->status = $request->status;
        $this->model->category_id = $request->category_id;
        $featured = "";
        if(isset($rules['featured'])){
            $path = $request->featured->store('images');
            if(file_exists(public_path($this->model->featured))){
                unlink(public_path($this->model->featured));
            }
            $featured = $path;
        }
        $doc = "";
        if(isset($rules['doc'])){
            $path = $request->doc->store('docs');
            if(file_exists(public_path($this->model->doc))){
                unlink(public_path($this->model->doc));
            }
            $doc = $path;
        }
        $this->model->featured = $featured;
        $this->model->doc = $doc;
        $this->model->save();
        if($this->model->type=="page"){
            return redirect()->route('admin.post.type',['type'=>'page'])->with(['success'=>"Page Updated"]);            
        }
        return redirect()->route('admin.post')->with(['success'=>"Post Updated"]);
    }
    public function destroy($id)
    {
        $this->model->find($id)->delete();
        return redirect()->route('admin.post')->with(['success'=>"Post Deleted"]);
    }
}
