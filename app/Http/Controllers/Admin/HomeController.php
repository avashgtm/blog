<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\Category as cmodel;
use App\Models\Post as pmodel;

class HomeController extends Controller
{
    protected $cmodel;
    protected $pmodel;
    public function __construct(cmodel $cmodel, pmodel $pmodel)
    {
        $this->cmodel = $cmodel;
        $this->pmodel = $pmodel;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catscount = $this->cmodel->count();
        $postscount = $this->pmodel->where('type','post')->count();
        $pagescount = $this->pmodel->where('type','page')->count();

        return view('admin.home')->with(compact('catscount','postscount','pagescount'));
    }
}
