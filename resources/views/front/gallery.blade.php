@extends('front.layouts.master')
@section('header')
<div class="blog-header">
  <h1 class="blog-title">Gallery</h1>
</div>
@stop
@section('content')
<div class="row">
  <div class='list-group gallery'>
    @foreach($images as $img) 
    <div class='col-sm-3'>
      <a class="thumbnail fancybox" rel="ligthbox" href="{{asset($img->image)}}" style="height:150px">
        <img class="img-responsive" alt="" src="{{asset($img->image)}}" />
        <div class='text-right'>
          <small class='text-muted'>{{$img->caption}}</small>
        </div> <!-- text-right / end -->
      </a>
    </div> <!-- col-6 / end -->
    @endforeach

  </div> <!-- list-group / end -->
</div> <!-- row / end -->
@endsection
@section('scripts')
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script>
  $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
      openEffect: "none",
      closeEffect: "none"
    });
  });
</script>
@endsection
